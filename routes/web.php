<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Duo2faController;
use App\Http\Controllers\SocialLoginController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\ShowActivityLogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/privacy-policy', function () {
    return view('policy');
});

Route::get('/terms-of-service', function () {
    return view('terms');
});

/* SOCIAL LOGIN ROUTES */
Route::get('social-login/{provider}', [SocialLoginController::class, 'redirect_to_provider'])->name('social-login.redirect');
Route::get('social-login/{provider}/callback', [SocialLoginController::class, 'callback_from_provider'])->name('social-login.callback');

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    /* DUO 2FA ROUTES */
    Route::get('/duo-2fa', [Duo2faController::class, 'resolve_duo_sign_request'])->name('duo.2fa');
    Route::post('/duo-2fa', [Duo2faController::class, 'resolve_duo_post_action']);

    Route::group(['middleware' => ['duo.2fa']], function () {
        /* DASHBOARD */
        Route::get('/dashboard', function () {
            return view('dashboard');
        })->name('dashboard');

        /* USER ROUTES */
        Route::get('user/data', [UserController::class, 'data'])->name('user.data');
        Route::resource('user', UserController::class);

        /* ACTIVITY LOG ROUTES */
        Route::get('activity-log', [ShowActivityLogController::class, 'index'])->name('activity-log.index');
        Route::get('activity-log/data', [ShowActivityLogController::class, 'data'])->name('activity-log.data');

        /* ROLE ROUTES */
        Route::get('role/data', [RoleController::class, 'data'])->name('role.data');
        Route::resource('role', RoleController::class);

        /* TASK ROUTES */
        Route::get('task/data', [TaskController::class, 'data'])->name('task.data');
        Route::resource('task', TaskController::class)->middleware('subscription.check');

        Route::get('subscription/payment', [SubscriptionController::class, 'subscription_payment'])->name('subscription.payment');
        Route::post('subscription/store', [SubscriptionController::class, 'subscription_store'])->name('subscription.store');
    });
});