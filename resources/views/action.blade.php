<div>
    @can("edit_$data->model")
        @if ($data->model == 'task')
            <a href="javascript:void(0)" data-id="{{ $data->user->id }}" class="btn btn-info btn-sm text-white kf_menu_edit">{{ __('Edit') }}</a>
        @else
            <a class="btn btn-info btn-sm text-white" href="{{ route($data->edit_url, $data->user->id) }}">
                {{ __('Edit') }}
            </a>
        @endif
    @endcan
    
    @can("delete_$data->model")
        @if ($data->model != 'user' || ($data->model == 'user' && auth()->id() != $data->user->id))
            <a class="sa-delete btn btn-danger btn-sm text-white" data-form-id="{{ $data->model.'-delete-form_'.$data->user->id }}">
                {{ __('Delete') }}
            </a>
            <form method="POST" id="{{ $data->model.'-delete-form_'.$data->user->id }}" action="{{ route($data->delete_url, $data->user->id) }}">
                @csrf
                @method('DELETE')
            </form>
        @endif
    @endcan
</div>