<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Role List') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-12 align-self-end">
            <a class="btn btn-primary" href="{{ route('role.create') }}">
                {{ __('Create Role') }}
            </a>
        </div>
        <div class="col-12">
            <div class="mt-4">
                <table id="basic-laratable" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>Created User</th>
                            <th>Updated User</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @push('style')
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    @endpush

    @push('scripts')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        {{-- <script src="//cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script> --}}

        <script>
            $(document).ready(function () {
                $.noConflict();
                $("#basic-laratable").DataTable({
                    serverSide: true,
                    ajax: "{{ route('role.data') }}",
                    columns: [
                        { name: 'name' },
                        { name: 'created_user.name', orderable: false },
                        { name: 'updated_user.name', orderable: false },
                        { name: 'created_at' },
                        { name: 'action', orderable: false, searchable:false},
                    ],
                });
            });
        </script>
    @endpush
</x-app-layout>

