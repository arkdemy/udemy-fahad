<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Create Role') }}
        </h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="card-body">
                <form method="POST" action="{{ route('role.store') }}">
                    @csrf
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Name') }}" />
        
                        <x-jet-input class="{{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name"
                                     :value="old('name')" required autofocus autocomplete="name" />
                        <x-jet-input-error for="name"></x-jet-input-error>
                    </div>
    
                    <h4 class="h4">
                        {{ __('Permissions: ') }}
                    </h4>
                    @if($errors->has('permissions'))
                        <span class="d-block invalid-feedback" role="alert">
                            <strong>{!! $errors->get('permissions')[0] !!}</strong>
                        </span>
                    @endif
                    <hr>

                    @foreach ($permissions as $permission)
                    <div class="form-group">
                            <h5>{{ $permission->name }}</h5>
                            <div class="row">
                                @foreach ($permission->children as $child)
                                    <div class="col-sm-6 col-md-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="permissions[]" id="ic_{{$child->id}}" value="{{ $child->id }}">
                                            <label class="custom-control-label" for="ic_{{$child->id}}">{{ ucwords(str_replace('_', ' ', $child->name))}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                        </div>
                    @endforeach
        
                    <div class="mb-0">
                        <div class="d-flex justify-content-end align-items-baseline">
                            <x-jet-button class="mr-2">
                                {{ __('Submit') }}
                            </x-jet-button>
                            <a class="btn btn-danger" href="{{ route('role.index') }}">
                                {{ __('Back') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
