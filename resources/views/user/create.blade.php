<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Create User') }}
        </h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="card-body">
                <form method="POST" action="{{ route('user.store') }}">
                    @csrf
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Name') }}" />
        
                        <x-jet-input class="{{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name"
                                     :value="old('name')" required autofocus autocomplete="name" />
                        <x-jet-input-error for="name"></x-jet-input-error>
                    </div>
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Email') }}" />
        
                        <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email"
                                     :value="old('email')" required />
                        <x-jet-input-error for="email"></x-jet-input-error>
                    </div>
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Password') }}" />
        
                        <x-jet-input class="{{ $errors->has('password') ? 'is-invalid' : '' }}" type="password"
                                     name="password" required autocomplete="new-password" />
                        <x-jet-input-error for="password"></x-jet-input-error>
                    </div>
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Confirm Password') }}" />
        
                        <x-jet-input class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
                    </div>
        
                    <div class="form-group">
                        <label for="select">{{ __('Role') }}</label>
       
                        <select name="roles[]" id="select" class="form-control" multiple required>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>

                        @if($errors->has('roles'))
                            <span class="d-block invalid-feedback" role="alert">
                                <strong>{!! $errors->get('roles')[0] !!}</strong>
                            </span>
                        @endif
                    </div>
        
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="active" value="active" name="status" class="custom-control-input" checked>
                            <label class="custom-control-label" for="active">ACTIVE</label>
                          </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="inactive" value="inactive" name="status" class="custom-control-input">
                            <label class="custom-control-label" for="inactive">INACTIVE</label>
                          </div>
                    </div>
        
                    <div class="mb-0">
                        <div class="d-flex justify-content-end align-items-baseline">
                            <x-jet-button class="mr-2">
                                {{ __('Submit') }}
                            </x-jet-button>
                            <a class="btn btn-danger" href="{{ route('user.index') }}">
                                {{ __('Back') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
