<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Create Task') }}
        </h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="card-body">
                <form method="POST" action="{{ route('task.store') }}">
                    @csrf
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Title') }}" />
        
                        <x-jet-input class="{{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title"
                                     :value="old('title')" required autofocus autocomplete="title" />
                        <x-jet-input-error for="title"></x-jet-input-error>
                    </div>
        
                    <div class="form-group">
                        <x-jet-label value="{{ __('Description') }}" />
                        <textarea name="description" id="description" rows="3" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" required>{{old('description')}}</textarea>
                        <x-jet-input-error for="description"></x-jet-input-error>
                    </div>
        
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="yes" value="yes" name="is_completed" class="custom-control-input">
                            <label class="custom-control-label" for="yes">YES</label>
                          </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="no" value="no" name="is_completed" class="custom-control-input" checked>
                            <label class="custom-control-label" for="no">NO</label>
                          </div>
                    </div>
        
                    <div class="mb-0">
                        <div class="d-flex justify-content-end align-items-baseline">
                            <x-jet-button class="mr-2">
                                {{ __('Submit') }}
                            </x-jet-button>
                            <a class="btn btn-danger" href="{{ route('task.index') }}">
                                {{ __('Back') }}
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
