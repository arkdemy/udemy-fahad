<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('View Task') }}
        </h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <div class="card-body">
                <div class="h4">{{ $task->title }}</div>
                <p>{{$task->description }}</p>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-end align-items-baseline">
                    <a class="btn btn-danger" href="{{ route('task.index') }}">
                        {{ __('Back') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
