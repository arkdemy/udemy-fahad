<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Task List') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-12 align-self-end">
            <a class="btn btn-primary" href="{{ route('task.create') }}">
                {{ __('Create Task') }}
            </a>
        </div>
        <div class="col-12">
            <div class="mt-4">
                <table id="basic-laratable" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Is Completed?</th>
                            <th>User</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="taskEditModal" tabindex="-1" role="dialog" aria-labelledby="taskEditModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Task</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST">
                    <div class="d-none alert alert-danger">
                        <ul class="kf_error">

                        </ul>
                    </div>

                    <div class="form-group d-none">
                        <label>Task ID<span class="required">*</span></label>
                        <div>
                            <input type="hidden" name="task_id" id="kf_task_id" class="form-control" placeholder="Task id" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Task Title<span class="required">*</span></label>
                        <div>
                            <input type="text" name="title" id="kf_title" class="form-control" placeholder="Task name" required/>
                        </div>
                    </div>

                    <div class="form-group" id="link-box">
                        <label>Description</label>
                        <div>
                            <textarea name="description" id="kf_description" rows="3" class="form-control" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="kf_yes" value="yes" name="is_completed" class="custom-control-input">
                            <label class="custom-control-label" for="kf_yes">YES</label>
                          </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="kf_no" value="no" name="is_completed" class="custom-control-input">
                            <label class="custom-control-label" for="kf_no">No</label>
                          </div>
                    </div>

                    <div class="form-group mb-0">
                        <div>
                            <button type="button" id="kf_submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>

                            <button data-dismiss="modal" aria-label="Close" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
    

    @push('style')
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    @endpush

    @push('scripts')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

        <script>
            $(document).ready(function () {
                $.noConflict();
                $("#basic-laratable").DataTable({
                    serverSide: true,
                    ajax: "{{ route('task.data') }}",
                    columns: [
                        { name: 'title' },
                        { name: 'description', orderable: false },
                        { name: 'is_completed' },
                        { name: 'user.name', orderable: false},
                        { name: 'created_at'},
                        { name: 'action', orderable: false, searchable:false},
                    ],
                });

                $(document).on('click', '.kf_menu_edit', function () {
                    $('.kf_error').parent().addClass('d-none');

                    let task_id = $(this).data('id');

                    axios.get(`/task/${task_id}`).then((res) => {
                        let data = res.data.task;
                        $('#kf_task_id').val(data.id);
                        $('#kf_title').val(data.title);
                        $('#kf_description').val(data.description);
                        if (data.is_completed == 'yes') {
                            $("#kf_yes").attr("checked", "checked");
                        } else {
                            $("#kf_no").attr("checked", "checked");
                        }

                        $("#taskEditModal").modal('show');
                    })
                })
                
                $(document).on('click', '#kf_submit', function () {
                    let task_id = $('#kf_task_id').val();
                    let data = {
                        'parent': $('#kf_parent').val(),			
                        'title': $('#kf_title').val(),			
                        'description': $('#kf_description').val(),			
                        'is_completed': $('input:checked').val(),			
                    };
                    console.log(data);

                    axios.put(`/task/${task_id}`, data).then((res) => {
                        Swal.fire({
                            title: "Updated?",
                            text: "Task updated successfully!",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#02a499",
                            confirmButtonText: "Ok!"
                        }).then(function (result) {
                            window.location.reload();
                        });
                    }).catch((err)=>{
                        let li = '';
                        let errors = err.response.data.errors;
                        for (const key in errors) {
                            if (Object.hasOwnProperty.call(errors, key)) {
                                const element = errors[key];
                                li += `<li>${element[0]}</li>`;
                            }
                        }
                        $('.kf_error').parent().removeClass('d-none').html(li);
                    });
                });
            });
        </script>
    @endpush
</x-app-layout>

