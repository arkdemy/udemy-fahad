<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Activity Log List') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-12">
            <div class="mt-4">
                <table id="basic-laratable" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Log Name</th>
                            <th>Subject</th>
                            <th>Subject Type</th>
                            <th>Causer</th>
                            <th>Created At</th>
                            <th>Properties</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @push('style')
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
        <style>
            pre {
                outline: 1px solid #ccc;
                padding: 5px;
                margin: 5px;
            }

            .string {
                color: green;
            }

            .number {
                color: darkorange;
            }

            .boolean {
                color: blue;
            }

            .null {
                color: magenta;
            }

            .key {
                color: red;
            }
        </style>
    @endpush

    @push('scripts')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        {{-- <script src="//cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script> --}}

        <script>
            $(document).ready(function () {
                $.noConflict();
                $("#basic-laratable").DataTable({
                    serverSide: true,
                    ajax: "{{ route('activity-log.data') }}",
                    columns: [
                        { name: 'log_name' },
                        { name: 'description' },
                        { name: 'subject_type' },
                        { name: 'causer_id' },
                        { name: 'created_at' },
                        { name: 'properties', orderable: false, searchable:false},
                    ],
                });
            });
        </script>
    @endpush
</x-app-layout>

