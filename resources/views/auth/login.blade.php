<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <div class="card-body">

            <x-jet-validation-errors class="mb-3 rounded-0" />

            @if (session('status'))
                <div class="alert alert-success mb-3 rounded-0" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <x-jet-label value="{{ __('Email') }}" />

                    <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email"
                                 name="email" :value="old('email')" required />
                    <x-jet-input-error for="email"></x-jet-input-error>
                </div>

                <div class="form-group">
                    <x-jet-label value="{{ __('Password') }}" />

                    <x-jet-input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password"
                                 name="password" required autocomplete="current-password" />
                    <x-jet-input-error for="password"></x-jet-input-error>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <x-jet-checkbox id="remember_me" name="remember" />

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>

                <div class="mb-0">
                    <div class="d-flex justify-content-end align-items-baseline">
                        @if (Route::has('password.request'))
                            <a class="text-muted mr-3" href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        @endif

                        <x-jet-button>
                            {{ __('Login') }}
                        </x-jet-button>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="mb-3">
            <div class="d-flex justify-content-center align-items-baseline">
                <a class="btn btn-info btn-block text-white text-bold" href="{{ route('social-login.redirect', 'facebook') }}">
                    {{ __('LOGIN WITH FACEBOOK') }}
                </a>
            </div>

            <div class="d-flex justify-content-center align-items-baseline my-3">
                <a class="btn btn-warning btn-block text-white text-bold" href="{{ route('social-login.redirect', 'google') }}">
                    {{ __('LOGIN WITH GOOGLE') }}
                </a>
            </div>

            <div class="d-flex justify-content-center align-items-baseline">
                <a class="btn btn-danger btn-block text-white text-bold" href="{{ route('social-login.redirect', 'github') }}">
                    {{ __('LOGIN WITH GITHUB') }}
                </a>
            </div>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>