<x-guest-layout>
    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-sm-12 col-md-6 my-5">
                <div>
                    <x-jet-authentication-card-logo />
                </div>

                <div>
                    <iframe id="duo_iframe">
                    </iframe>
                
                    <form method="POST" id="duo_form">
                        @csrf
                        <input type="hidden" name="next" value="next" />
                    </form>
                </div>
            
                <script src="/js/vendor/duo/duo_script.min.js"></script>
                <script>
                    let host = "{{ $host }}";
                    let sig_request = "{{ $response }}";
                    Duo.init({
                      'host': host,
                      'sig_request': sig_request,
                      'post_action': ""
                    });
                </script>
            </div>
        </div>
    </div>    
</x-guest-layout>