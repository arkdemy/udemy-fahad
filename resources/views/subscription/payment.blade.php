<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Subscription Payment') }}
        </h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="card-body">
                <form>
                    @csrf

                    <div class="form-group">
                        <select name="plan" id="plan" class="form-control">
                            <option value="" disabled>Select plan</option>
                            @foreach ($plans as $item)
                                <option value="{{ $item['key'] }}">{{ $item['value'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input id="card-holder-name" class="form-control" type="text" value="{{auth()->user()->name}}">
                    </div>

                    <!-- Stripe Elements Placeholder -->
                    <div id="card-element" class="form-group"></div>

                    <button class="btn btn-info mt-2 text-white" id="card-button" data-secret="{{ $intent->client_secret }}">
                        Subscribe
                    </button>

                    <script src="https://js.stripe.com/v3/"></script>

                    <script>
                        const stripe = Stripe("{{ env('STRIPE_KEY') }}");

                        const elements = stripe.elements();
                        const cardElement = elements.create('card');

                        cardElement.mount('#card-element');

                        window.addEventListener('load', () => {
                            const cardHolderName = document.getElementById('card-holder-name');
                            const plan = document.getElementById("plan").value;
                            const cardButton = document.getElementById('card-button');
                            const clientSecret = cardButton.dataset.secret;
    
                            cardButton.addEventListener('click', async (e) => {
                                e.preventDefault();
                                const { setupIntent, error } = await stripe.confirmCardSetup(
                                    clientSecret, {
                                        payment_method: {
                                            card: cardElement,
                                            billing_details: { name: cardHolderName.value }
                                        }
                                    }
                                );
    
                                if (error) {
                                    console.log('Error');
                                } else {
                                    /* AXIOS CALL TO STORE THE SUBSCRIPTION */
                                    axios.post('/subscription/store', { 
                                        'plan': plan,
                                        'payment_method': setupIntent.payment_method,
                                    }).then((res) => {
                                        window.location = "{{ route('dashboard') }}";
                                    })
                                }
                            });
                        })
                    </script>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
