<x-guest-layout>
    <div class="row justify-content-center pt-4">
        <div class="col-6">
            <div>
                <x-jet-authentication-card-logo />
            </div>

            <div class="card shadow-sm">
                <div class="card-body">
                    {{-- {!! $terms !!} --}}
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam provident adipisci at necessitatibus, doloremque ipsum illum sunt nam debitis alias similique ab. Nihil neque temporibus sunt minus totam voluptatem magnam.
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>