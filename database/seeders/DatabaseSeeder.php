<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* One way of seed DB using factory */
        User::factory(10)->create();

        /* Another way of seed DB using SEEDER CLASS */
        $this->call([
            RoleAndPermissionSeeder::class, // no need to import the class
        ]);
    }
}