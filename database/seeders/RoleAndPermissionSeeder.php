<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parentPermissions = [
            "Activity Log",
            "Role",
            "Task",
            "User",
        ];

        $childPermissions = array(
            'Activity Log' => array(
                0 => 'show_activity_log',
            ),
            'Role' => array(
                0 => 'add_role',
                1 => 'edit_role',
                2 => 'show_role',
                3 => 'delete_role',
            ),
            'Task' => array(
                0 => 'add_task',
                1 => 'edit_task',
                2 => 'show_task',
                3 => 'delete_task',
            ),
            'User' => array(
                0 => 'add_user',
                1 => 'edit_user',
                2 => 'show_user',
                3 => 'delete_user',
            ),
        );


        /* GET/CREATE A NEW USER */
        $user = User::firstOrCreate([
            'name' => 'Super Admin', 'email' => 'admin@gmail.com', 'password' => Hash::make('password'), 'phone_number' => '+880 1642 157515'
        ]);

        /* FOR SHOWING MESSAGE IN THE COMMAND PROMPT */
        $this->command->info('New user: ' . $user->name . ' with Email: ' . $user->email . ' & Password: password - was created successfully.');

        foreach (Role::availableRoles() as $availableRole) {
            /* CREATE ROLE FROM ROLE MODEL- availableRoles ARRAY */
            $role = Role::create([
                'name' => $availableRole,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ]);
            /* FOR SHOWING MESSAGE IN THE COMMAND PROMPT */
            $this->command->info('Role: ' . $role->name . ' created successfully on ' . Carbon::now()->format('F j, Y, g:i:s A'));
        }

        Permission::query()->delete();
        foreach ($parentPermissions as $parent) {
            /* CREATE A PARENT PERMISSION FROM THE PARENT PERMISSIONS ARRAY */
            $parentPermission = Permission::create([
                'name' => $parent,
                'parent_id' => '',
                'guard_name' => 'web'
            ]);
            if (key_exists($parent, $childPermissions)) {
                foreach ($childPermissions[$parent] as $childPermission) {
                    /* CREATE A CHILD PERMISSION FROM THE CHILD PERMISSIONS ARRAY*/
                    $permission = Permission::create([
                        'name' => $childPermission,
                        'parent_id' => $parentPermission->id,
                        'guard_name' => 'web'
                    ]);

                    /* ASSIGN THE PERMISSION TO ROLE `SUPER ADMIN` */
                    $permission->assignRole('Super Admin');
                }
            }
        }

        /* FOR SHOWING MESSAGE IN THE COMMAND PROMPT */
        $this->command->info('Permissions was created successfully.');

        /* ASSIGN ROLE TO USER */
        $user->assignRole('Super Admin');

        /* FOR SHOWING MESSAGE IN THE COMMAND PROMPT */
        $this->command->info($user->name . ' has assigned role: ' . $user->getRoleNames());
    }
}