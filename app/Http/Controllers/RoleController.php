<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:add_role', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_role',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:show_role',   ['only' => ['show', 'index']]);
        $this->middleware('permission:delete_role',   ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('role.index');
    }

    /**
     * return a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        return Laratables::recordsOf(Role::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('parent_id', '')->with('children')->get();

        return view('role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:roles'],
            'permissions' => ['required', 'array', 'min:1'],
        ])->validate();

        $role = new Role();
        $role->name = $request->name;
        $role->guard_name = 'web';
        $role->created_by = auth()->id();
        $role->save();

        $role->syncPermissions($request->permissions);

        return redirect()->route('role.index')->with('success', 'A new role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id', $id)->with('permissions')->firstOrFail();

        $permissions = Permission::where('parent_id', '')->with('children')->get();

        $role_permission = $role->permissions->pluck('id')->toArray();

        return view('role.edit', compact('permissions', 'role', 'role_permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:roles,name,' . $id],
            'permissions' => ['required', 'array', 'min:1'],
        ])->validate();

        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->updated_by = auth()->id();
        $role->save();

        $role->syncPermissions($request->permissions);
        return redirect()->route('role.index')->with('success', 'Role information updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->route('role.index')->with('success', 'Role information deleted successfully');
    }
}