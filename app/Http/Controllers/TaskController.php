<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\TaskNotification;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\Response;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:add_task', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_task',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:show_task',   ['only' => ['show', 'index']]);
        $this->middleware('permission:delete_task',   ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('task.index');
    }

    /**
     * return a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        return Laratables::recordsOf(Task::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ])->validate();

        $task = Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'is_completed' => $request->is_completed,
            'user_id' => Auth::id(),
        ]);

        Notification::route('nexmo', Auth::user()->phone_number ?? '+880 1642 157515')
            ->route('mail', Auth::user()->email ?? 'taylor@example.com')
            ->route('slack', env('SLACK_HOOK_URL'))
            ->route(MicrosoftTeamsChannel::class, env('TEAMS_WEBHOOK_URL'))
            ->notify(new TaskNotification($task));

        return redirect()->route('task.index')->with('success', 'A new task created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if (request()->ajax()) {
            return response()->json(['task' => $task], Response::HTTP_OK);
        }
        return view('task.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $task = Task::with('user')->where('id', $task->id)->first();
        return view('task.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ])->validate();

        $task->title = $request->title;
        $task->description = $request->description;
        $task->is_completed = $request->is_completed;

        $task->save();

        if ($request->ajax()) {
            return response()->json(['task' => $task], Response::HTTP_ACCEPTED);
        }

        return redirect()->route('task.index')->with('success', 'Task information updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()->route('task.index')->with('success', 'Task information deleted successfully');
    }
}