<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    /**
     * Function to redirect to the login page
     * of specific provider like FB, GITHUB, etc.
     *
     * @param [type] $provider
     * @return void
     */
    public function redirect_to_provider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Function to handle after provider authentication
     *
     * @param [type] $provider
     * @return void
     */
    public function callback_from_provider($provider)
    {
        try {
            $user_info = Socialite::driver($provider)->user();
            $user = User::where(['email' => $user_info->getEmail()])->first();
            if (!$user) {
                $user = User::create([
                    'name' => $user_info->getName(),
                    'email' => $user_info->getEmail(),
                    'profile_photo_path' => $user_info->getAvatar(),
                    'provider' => $provider,
                    'provider_id' => $user_info->getId()
                ]);
            }
            Auth::login($user);
            activity()->log("$user->name logged in to the system");
        } catch (\Throwable $th) {
            Log::debug('LOGIN ERROR: ' . $th->getMessage());
        }
        return redirect()->route('dashboard');
    }
}