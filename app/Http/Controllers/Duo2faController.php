<?php

namespace App\Http\Controllers;

use Duo\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Duo2faController extends Controller
{
    public $aKey;
    public $iKey;
    public $sKey;
    public $hName;

    public function __construct()
    {
        $this->aKey = env('DUO_AUTH_KEY');
        $this->iKey = env('DUO_INTEGRATION_KEY');
        $this->sKey = env('DUO_SECURITY_KEY');
        $this->hName = env('DUO_HOST_NAME');
    }

    /**
     * Resolve sign request for duo
     * It will generate a sign request
     * and return view where iframe for 2fa will trigger
     * 
     * @return void
     */
    public function resolve_duo_sign_request()
    {
        $response = Web::signRequest($this->iKey, $this->sKey, $this->aKey, Auth::user()->email);
        $host = $this->hName;
        return view('auth.duo', compact('response', 'host'));
    }

    /**
     * Resolve post action after successful 2fa via DUO
     *
     * @param Request $request
     * @return void
     */
    public function resolve_duo_post_action(Request $request)
    {
        $request->validate([
            'sig_response' => 'required',
        ]);
        $verified_user = Web::verifyResponse($this->iKey, $this->sKey, $this->aKey, $request->sig_response);

        Session::put('user_2fa', $verified_user);

        return redirect('/dashboard');
    }
}