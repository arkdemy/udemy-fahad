<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;
use Freshbitsweb\Laratables\Laratables;

class ShowActivityLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:show_activity_log',   ['only' => ['show', 'index']]);
    }

    public function index()
    {
        // return Activity::all();
        return view('activity_log.index');
    }

    /**
     * return a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        return Laratables::recordsOf(Activity::class);
    }
}