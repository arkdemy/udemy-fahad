<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function subscription_payment()
    {
        $plans = [
            [
                'key' => 'price_1IGTMHEhFfvWxiQMXfbxy3m4',
                'value' => 'Basic Plan (£5/M)'
            ],
            [
                'key' => 'price_1IGTN6EhFfvWxiQM0yCpp74g',
                'value' => 'Life Time (£100/LT)'
            ]
        ];
        $intent = auth()->user()->createSetupIntent();

        return view('subscription.payment', compact('plans', 'intent'));
    }

    public function subscription_store(Request $request)
    {
        $user = auth()->user();

        $user->newSubscription(
            'default',
            $request->plan
        )->create($request->payment_method);

        return response()->json(['data' => 'Subscription created successfully.'], 200);
    }
}