<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateActivityLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getRequestUri() == '/logout') {
            $user = Auth::user();
            activity()->log("$user->name logged out from the system");
        } else if ($request->getRequestUri() == '/login' && $request->method() == 'POST') {
            $user = User::where([
                ['email', $request->email],
                ['status', User::ACTIVE]
            ])->first();
            if ($user) {
                activity()->log("$user->name logged in to the system");
            }
        }

        return $next($request);
    }
}