<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Models\Activity as ActivityLog;

class Activity extends ActivityLog
{
    use HasFactory;

    /**
     * Display currency symbol with format in salary column value.
     *
     * @param  \Spatie\Activitylog\Models\Activity
     * @return  string
     */
    public static function laratablesCauserId($activity)
    {
        return $activity->causer_id ? User::find($activity->causer_id)->name : '';
    }

    /**
     * Returns the properties Column html for datatables.
     *
     * @param  \Spatie\Activitylog\Models\Activity
     * @return  view
     */
    public static function laratablesCustomProperties($activity)
    {
        $activity = parent::find($activity->id);
        return view('activity_log.properties', compact('activity'))->render();
    }
}