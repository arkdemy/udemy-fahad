<?php

namespace App\Models;

use stdClass;
use App\Models\User;
use Spatie\Permission\Models\Role as SpatieRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends SpatieRole
{
    use HasFactory;

    /**
     * Mutates the role name before storing into the DB
     * 
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    /**
     * @return array
     */
    public static function availableRoles()
    {
        return [
            'Super Admin' // You can add as many roles here also for seeding
        ];
    }

    /**
     * Created user belongs to User
     *
     * @return void
     */
    public function created_user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Updated user belongs to User
     *
     * @return void
     */
    public function updated_user()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /**
     * Returns the action Column html for datatables.
     *
     * @param  \App\User
     * @return  string
     */
    public static function laratablesCustomAction($role)
    {
        $data = new stdClass();
        $data->user = $role;
        $data->model = 'role';
        $data->edit_url = 'role.edit';
        $data->delete_url = 'role.destroy';
        return view('action', compact('data'))->render();
    }
}