<?php

namespace App\Models;

use stdClass;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'is_completed', 'user_id'];

    /**
     * A task is belongs to an user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Display is_completed in formattd way.
     *
     * @param  \App\Task
     * @return  string
     */
    public static function laratablesIsCompleted($task)
    {
        return $task->is_completed == 'yes' ? "<span class='badge badge-success'>" . strtoupper($task->is_completed) . "</span>" : "<span class='badge badge-danger'>" . strtoupper($task->is_completed) . "</span>";
    }

    /**
     * Returns the action Column html for datatables.
     *
     * @param  \App\Task
     * @return  string
     */
    public static function laratablesCustomAction($task)
    {
        $data = new stdClass();
        $data->user = $task;
        $data->model = 'task';
        $data->edit_url = 'task.edit';
        $data->delete_url = 'task.destroy';
        return view('action', compact('data'))->render();
    }
}