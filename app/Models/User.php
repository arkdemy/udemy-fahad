<?php

namespace App\Models;

use stdClass;
use App\Models\Task;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use LogsActivity;
    use HasRoles;
    use Billable;

    /* USER STATUS */
    const ACTIVE = "active";
    const INACTIVE = "inactive";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'provider',
        'provider_id',
        'status'
    ];

    /* NAME OF THE LOG */
    protected static $logName = 'user';
    /* WHICH ATTRIBUTES WILL IGNORE WHILE LOGGING */
    protected static $ignoreChangedAttributes = ['password', 'updated_at'];
    /* LOGGING ONLY THE CHANGED ATTRIBUTES */
    protected static $logOnlyDirty = true;
    /* LOG FOR WHICH ATTRIBUTES  */
    protected static $logAttributes = ['name', 'email', 'status'];
    /* CUSTOM DESCRIPTION OF THE LOG */
    public function getDescriptionForEvent(string $eventName): string
    {
        return "User has been {$eventName}!";
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * An user has many tasks
     *
     * @return void
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'user_id', 'id');
    }

    /**
     * Display currency symbol with format in salary column value.
     *
     * @param  \App\User
     * @return  string
     */
    public static function laratablesStatus($user)
    {
        return $user->status == self::ACTIVE ? "<span class='badge badge-success'>" . strtoupper($user->status) . "</span>" : "<span class='badge badge-danger'>" . strtoupper($user->status) . "</span>";
    }

    /**
     * Returns the action Column html for datatables.
     *
     * @param  \App\User
     * @return  string
     */
    public static function laratablesCustomAction($user)
    {
        $data = new stdClass();
        $data->user = $user;
        $data->model = 'user';
        $data->edit_url = 'user.edit';
        $data->delete_url = 'user.destroy';
        return view('action', compact('data'))->render();
    }
}