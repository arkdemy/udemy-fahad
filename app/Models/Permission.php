<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as SpatiePermission;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permission extends SpatiePermission
{
    use HasFactory;

    /**
     * A parent permission has many child permission
     *
     * @return void
     */
    public function children()
    {
        return $this->hasMany(Permission::class, 'parent_id');
    }
}